# VBA

#### 介绍
偶然的机会，一位老师提出的需求，通过弧长和弦长计算圆半径。
此前没用过VBA，因为老师的原因务必强迫一下自己。最后在Excel中使用VBA中实现了。
在Excel中使用VBA 代码，通过鼠标右键触发计算，通过弧长和弦长计算圆半径。

#### 软件架构
仅仅是Excel 中使用 一段 VBA 代码


#### 安装教程

无需安装

#### 使用说明
在Excel中启用宏，鼠标右键启动计算。

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
